#pragma once

#include "Light.h"
#include <map>
#include <vector>
#include <optional>
#include <cstddef>


class Board
{
public:
	Board() = default;
	~Board() = default;
	using Position = std::pair<uint8_t, uint8_t>;
	using Type = std::pair<uint8_t, uint8_t>;

	// Getter (returning immutable object)
	const Light& GetElementAt(const Position&) const;
	// Getter and/or Setter
	Light& ElementAt(const Position&);

	Type& GetType();

	void SetColorForLightOn(const Light::Color&);
	void SetColorForLightOff(const Light::Color&);

	Board& UpdateLight(const Position&);
	Board& UpdateUpLight(const Position&);
	Board& UpdateDownLight(const Position&);
	Board& UpdateLeftLight(const Position&);
	Board& UpdateRightLight(const Position&);
	Board& UpdateDiagonalUpRightLight(const Position&);
	Board& UpdateDiagonalUpLeftLight(const Position&);
	Board& UpdateDiagonalDownRightLight(const Position&);
	Board& UpdateDiagonalDownLeftLight(const Position&);

	std::vector<std::vector<Light>> GetLights();

	friend std::istream& operator >> (std::istream&, Board&);
	friend std::ostream& operator << (std::ostream&, const Board&);

	void EmptyBoard(const Type&);
	bool Finish();
	void DeleteBoard();
	void SetTimeToResolveBoard(const double& time = 90.00);
	double& GetTimeToResolveBoard();
private:
	std::vector<std::vector<Light>> m_lights;
	std::map<Position, Light> m_lightsOn;
	void CheckLightsOn(const Board::Position&);
	double m_timeToResolveBoard;
	Light::Color m_colorLightOn, m_colorLightOff;
};
