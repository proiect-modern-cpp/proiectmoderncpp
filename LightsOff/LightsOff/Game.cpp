#pragma once
#include "Game.h"
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

Game::Game()
{
	std::cout << "\n" << std::setw(60) << std::setiosflags(std::ios::right) << "LightsOff" << "\n\n\n";
}

void Game::GameLoop(uint32_t option, Player& player, Logger& logger)
{
	while (!m_board.Finish())
	{
		system("cls");
		std::cout << std::endl << m_board << std::endl;
		Board::Position pos;
		while (true)
		{
			try
			{
				pos = player.SelectLight(m_board);
				GameUtilities<Board, Light>::ApplyRule(m_board, option, pos);
				break;
			}
			catch (const std::runtime_error& error)
			{
				std::stringstream logStream;
				logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
				logger.log(logStream.str(), Logger::Level::Warning);
				logStream.str("");
				std::cerr << "\n" << "Invalid position, try again...\n";
			}
		}
	}
}

void Game::Run(Logger& logger)
{
	Player player;
	player.ReadName();

	std::stringstream logStream;

	uint32_t option;
	while (true)
	{
		try
		{
			option = std::move(player.SelectGameMode());
			break;
		}
		catch (const std::runtime_error& error)
		{
			logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
			logger.log(logStream.str(), Logger::Level::Warning);
			logStream.str("");
			std::cerr << "\n" << "Invalid input, try again...\n";
		}
	}

	while (true)
	{
		try
		{
			player.SelectColors(m_board);
			logStream << "The user with the username " << player << " has chosen the colors for the lights. ";
			logger.log(logStream.str(), Logger::Level::Info);
			logStream.str("");
			break;
		}
		catch (const std::runtime_error& error)
		{
			logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
			logger.log(logStream.str(), Logger::Level::Warning);
			logStream.str("");
			std::cerr << "Invalid color, try again..." << std::endl;
		}
	}
	bool play = true;
	std::string gameMode;
	while (option < kExit)
	{
		if (option == kFirstOption)
		{
			GameUtilities<Board, Light>::MenuGameModes();
			while (true)
			{
				try
				{
					option = std::move(player.SelectGameSubmode());
					gameMode = std::move(GameUtilities<Board, Light>::GetGameMode(option));
					break;
				}
				catch (const std::runtime_error& error)
				{
					logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
					logger.log(logStream.str(), Logger::Level::Warning);
					logStream.str("");
					std::cerr << "\n" << "Invalid input, try again...\n";
				}
			}

			uint32_t lenght, width;
			while (true)
			{
				try
				{
					player.ReadDimensions(lenght, width);
					break;
				}
				catch (const std::runtime_error& error)
				{
					logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
					logger.log(logStream.str(), Logger::Level::Warning);
					logStream.str("");
					std::cerr << "\n" << "Invalid dimensions, try again...\n";
				}
			}
			logStream << "The user with the username " << player << " has chosen to play a " << lenght << "x" << width << " game with" << gameMode << ".";
			logger.log(logStream.str(), Logger::Level::Info);
			logStream.str("");
			if (GameUtilities<Board, Light>::IsPredefinedType({ lenght,width }) && option == GameUtilities<Board, Light>::kClassicGameMode)
				GameUtilities<Board, Light>::GetBoard(m_board, { lenght, lenght });
			else
				GameUtilities<Board, Light>::CreateUndefinedBoard(m_board, { lenght,width }, option);
		}
		else
		{
			logStream << "The user with the username " << player << " has chosen to create a table.";
			logger.log(logStream.str(), Logger::Level::Info);
			logStream.str("");
			option = GameUtilities<Board, Light>::kClassicGameMode;
			CreateBoard(GameUtilities<Board, Light>::GetPredefinedType(1), player, logger);
			if (GameUtilities<Board, Light>::SolvableBoard(m_board))
				m_board.SetTimeToResolveBoard();
			else
				play = false;
		}


		if (play == true)
		{
			bool fastModeOption;
			while (true)
			{
				try
				{
					fastModeOption = player.SelectFastMode();
					break;
				}
				catch (const std::runtime_error& error)
				{
					std::stringstream logStream;
					logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
					logger.log(logStream.str(), Logger::Level::Warning);
					logStream.str("");
					std::cerr << "\n" << "Invalid input, try again...\n";
				}
			}
			clock_t begin, end;
			double time_spent;
			begin = clock();

			GameLoop(option, player, logger);

			end = clock();
			time_spent = static_cast<double>(end) - static_cast<double>(begin) / CLOCKS_PER_SEC;
			AnalyzeGame(time_spent, fastModeOption, logger);
		}
		else
		{
			std::cout << "This table is not solvable!\n";
			play = true;
		}


		m_board.DeleteBoard();
		std::cout << "Enter 1 to play on a predefined;\n" << "Enter 2 to create your own table;\n" << "Enter 3 to exit.\n";
		while (true)
		{
			try {
				option = std::move(player.CheckInput("[1-3]"));
				break;
			}
			catch (const std::runtime_error& error)
			{
				logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
				logger.log(logStream.str(), Logger::Level::Warning);
				logStream.str("");
				std::cerr << "\n" << "Invalid input, try again...\n";
			}
		}
		system("cls");
	}
}

void Game::CreateBoard(const Board::Type& type, Player& player, Logger& logger)
{
	m_board.EmptyBoard(type);
	auto& [line, column] = type;
	std::cout << m_board;
	int option = kFirstOption;
	std::stringstream logStream;
	while (option < kSecondOption)
	{
		if (option == kFirstOption)
		{
			while (true)
			{
				try
				{
					Board::Position pos = player.SelectLight(m_board);
					m_board.UpdateLight(pos);
					system("cls");
					break;
				}
				catch (const std::runtime_error& error)
				{
					logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
					logger.log(logStream.str(), Logger::Level::Warning);
					logStream.str("");
					std::cerr << "\n" << "Invalid position, try again...\n";
				}
			}
		}
		std::cout << m_board;
		std::cout << std::endl << "Enter 1 if you want to change the state of another light; \n";
		std::cout << "Enter 2 to exit. \n";
		while (true)
		{
			try
			{
				option = std::move(player.CheckInput("[1-2]"));
				break;
			}
			catch (const std::runtime_error& error)
			{
				logStream << "The user with the username " << player << " has given a wrong input: " << error.what();
				logger.log(logStream.str(), Logger::Level::Warning);
				logStream.str("");
				std::cerr << "\n" << "Invalid input, try again...\n";
			}
		}
	}
}

void Game::AnalyzeGame(double& time, bool fastModeOption, Logger& logger)
{
	system("cls");
	uint16_t minutes = time / 60;
	uint16_t seconds = std::fmod(time, 60);
	std::stringstream logStream;

	std::cout << "The game lasted " << minutes << " minutes and " << seconds << " seconds.\n";
	logStream << "The game lasted " << minutes << " minutes and " << seconds << " seconds. ";

	if (fastModeOption == true && time > m_board.GetTimeToResolveBoard())
	{
		std::cout << "You haven't finished the game in time! :(\n";
		logStream << "Game lost.";
		logger.log(logStream.str(), Logger::Level::Info);
		return;
	}
	std::cout << "Congratulations, you won!\n";
	logStream << "Game wom.";
	logger.log(logStream.str(), Logger::Level::Info);
}

