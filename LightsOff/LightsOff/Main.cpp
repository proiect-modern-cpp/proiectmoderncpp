#pragma once
#include "Board.h"
#include "Game.h"
#include <fstream>
#include <stdlib.h>     
#include <time.h>


int main()
{
	std::ofstream os("syslog.log", std::ios::app);
	Logger logger(os);
	logger.log("Application started.", Logger::Level::Info);
	Game game;
	srand(time(0));
	game.Run(logger);
	logger.log("Application stopped.", Logger::Level::Info);
	return 0;
}