#include "Light.h"
#include "rang.hpp"

Light::Light(const Color& color, const State& state) : m_color(color), m_state(state) {}

Light& Light::UpdateLight(const Color& on, const Color& off)
{
	if (m_state == Light::State::ON)
	{
		m_color = off;
		m_state = Light::State::OFF;
	}
	else
	{
		m_color = on;
		m_state = Light::State::ON;
	}
	return *this;
}

Light::Color Light::GetColor() const
{
	return m_color;
}

Light::State Light::GetState() const
{
	return m_state;
}

std::ostream& operator<<(std::ostream& print, const Light& circle)
{
	switch (circle.m_color)
	{
	case Light::Color::RED: print << rang::bg::red; break; 
	case Light::Color::BLUE: print << rang::bg::blue; break;
	case Light::Color::GREEN: print << rang::bg::green; break;
	case Light::Color::YELLOW: print << rang::bg::yellow; break;
	case Light::Color::CYAN: print << rang::bg::cyan; break;
	case Light::Color::MAGENTA: print << rang::bg::magenta; break;
	default:
		break;
	}
	return print <<  static_cast<uint16_t>(circle.m_color) << static_cast<uint16_t>(circle.m_state) << " "<<rang::bg::reset;
}
