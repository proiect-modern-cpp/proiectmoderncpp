#pragma once
#include "../Logger/Logger.h"
#include <iostream>
#include <fstream>
#include "Board.h"
#include "Player.h"
#include "GameUtilities.h"
#include <map>
#include <array>
#include <sstream>
#include <regex>

class Game
{
private:
	Board m_board;

	void GameLoop(uint32_t, Player&, Logger&);

	void CreateBoard(const Board::Type&, Player&, Logger&);

	void AnalyzeGame(double&, bool, Logger&);

public:
	Game();
	~Game() = default;
	void Run(Logger&);
	static const uint32_t kFirstOption = 1, kSecondOption = 2, kExit = 3;
};

