#include "Player.h"
#include <array>
#include <sstream>
#include <regex>

Player::Player(const std::string& name) :m_name{ name } {}

Board::Position Player::SelectLight(Board& board)
{
	std::string lineIn, columnIn;
	std::cout << m_name << ", enter the position of the light you want to change: ";

	std::cin >> std::skipws >> lineIn >> columnIn;

	std::string input;
	std::getline(std::cin, input);
	input.erase(std::remove_if(input.begin(), input.end(), ::isspace), input.end());
	if (!input.empty()) 
		throw std::runtime_error("Too much input. ");
	if (!std::regex_match(lineIn, std::regex("[0-9]+")) || !std::regex_match(columnIn, std::regex("[0-9]+")))
		throw std::runtime_error("Integer expected, actual inputs: " + lineIn + ", " + columnIn + ".");

	std::stringstream lineStream(lineIn), columnStream(columnIn);
	uint32_t line, column;
	lineStream >> line;
	columnStream >> column;
	return { line, column };
	
}

void Player::SelectColors(Board& board)
{
	std::string colorOn, colorOff;

	std::map<std::string, Light::Color> colors;
	colors["red"] = Light::Color::RED;
	colors["blue"] = Light::Color::BLUE;
	colors["green"] = Light::Color::GREEN;
	colors["yellow"] = Light::Color::YELLOW;
	colors["cyan"] = Light::Color::CYAN;
	colors["magenta"] = Light::Color::MAGENTA;

	std::cout << std::endl;
	std::cout << "\nAvailable colors: \n";
	std::cout << "red, blue, green, yellow, cyan, magenta \n";
	std::cout << "Color for the light on: ";
	std::cin >> colorOn;
	if (colors.find(colorOn) == colors.end())
		throw std::runtime_error("Invalid color.");
	do {
		std::cout << "Color for the light off: ";
		std::cin >> colorOff;
	} while (colorOn == colorOff);
	if (colors.find(colorOff) == colors.end())
		throw std::runtime_error("Invalid color.");
	system("cls");
	board.SetColorForLightOn(std::move(colors[colorOn]));
	board.SetColorForLightOff(std::move(colors[colorOff]));
}

uint32_t Player::SelectGameMode()
{
	std::string input;
	std::cout << "\nTo play a predefined table enter 1!\n";
	std::cout << "To create your own table enter 2!\n";
	std::cin >> std::skipws >> input;
	
	std::string s;
	std::getline(std::cin, s);
	s.erase(std::remove_if(s.begin(), s.end(), ::isspace), s.end());
	if (!s.empty()) 
		throw std::runtime_error("Too much input.");

	if (!std::regex_match(input, std::regex("[0-9]+")))
		throw std::runtime_error("Integer expected, actual input: " + input + ".");
	std::stringstream ss(input);
	uint32_t option;
	ss >> option;

	if (option < kFirstOption || option > kSecondOption)
		throw std::runtime_error("Invalid option!");
	system("cls");
	return option;

}

uint32_t Player::SelectGameSubmode()
{
	std::string input;
	std::cout << "\nEnter the game mode you want to play: ";
	std::cin >> std::skipws >> input;

	std::string s;
	std::getline(std::cin, s);
	s.erase(std::remove_if(s.begin(), s.end(), ::isspace), s.end());
	if (!s.empty())
		throw std::runtime_error("Too much input");

	if (!std::regex_match(input, std::regex("[0-9]+")))
		throw std::runtime_error("Integer expected, actual input: " + input + ".");

	std::stringstream stream(input);
	uint32_t option;
	stream >> option;

	if (!std::regex_match(input, std::regex("[1-6]")))
		throw std::runtime_error("Invalid option!");
	system("cls");
	return option;
}

bool Player::SelectFastMode()
{
	std::string fastModeOption;
	std::cout << "\nDo you want to play in fast mode? (Y/N) ";
	std::cin >> fastModeOption;
	if (fastModeOption == "y" || fastModeOption == "Y")
		return true;
	if (fastModeOption == "n" || fastModeOption == "N")
		return false;
	throw std::runtime_error("Invalid option!");
}

void Player::ReadName()
{
	std::string name;
	std::cout << "\nEnter your name: ";
	std::getline(std::cin, name);
	m_name = name;
	system("cls");
}

void Player::ReadDimensions(uint32_t& lenght, uint32_t& width)
{
	std::cout << "\nEnter the dimensions for the table (between 3 and 9): ";
	std::string inLenght, inWidth;
	std::cin >> std::skipws >> inLenght >> inWidth;

	std::string stream;
	std::getline(std::cin, stream);
	stream.erase(std::remove_if(stream.begin(), stream.end(), ::isspace), stream.end());
	if (!stream.empty()) throw std::runtime_error("Too much input. ");

	if (!std::regex_match(inLenght, std::regex("[0-9]+")) || !std::regex_match(inWidth, std::regex("[0-9]+")))
		throw std::runtime_error("Integer expected, actual inputs: " + inLenght + ", " + inWidth + ".");
	if (!std::regex_match(inLenght, std::regex("[3-9]")) || !std::regex_match(inWidth, std::regex("[3-9]")))
		throw std::runtime_error("Invalid dimensions!");

	std::stringstream lenghtStream(inLenght), widthStream(inWidth);
	lenghtStream >> lenght;
	widthStream >> width;
	std::cout << std::endl;
	system("cls");
}

uint32_t Player::CheckInput(const std::string& pattern)
{
	uint32_t option;
	std::string input;
	std::cin >> std::skipws >> input;
	std::string aux;
	std::getline(std::cin, aux);
	aux.erase(std::remove_if(aux.begin(), aux.end(), ::isspace), aux.end());
	if (!aux.empty())
		throw std::runtime_error("Too much input. ");
	if (!std::regex_match(input, std::regex("[0-9]+")))
		throw std::runtime_error("Integer expected, actual inputs: " + input);
	if (!std::regex_match(input, std::regex(pattern)))
		throw std::runtime_error("Invalid option.");
	std::stringstream stream(input);
	stream >> option;
	return option;
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
	return os << player.m_name;
}
