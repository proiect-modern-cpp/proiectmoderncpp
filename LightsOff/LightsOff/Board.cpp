#include "Board.h"

const Light& Board::GetElementAt(const Position& pos) const
{
	const auto& [line, column] = pos;
	if (line >= m_lights.size() || line < 0 || column >= m_lights[0].size() || column < 0)
		throw std::runtime_error("Invalid position!");
	return m_lights[line][column];
}

Light& Board::ElementAt(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line >= m_lights.size() || line < 0 || column >= m_lights[0].size() || column < 0)
		throw std::runtime_error("Invalid position!");
	return m_lights[line][column];
}

Board::Type& Board::GetType()
{
	Board::Type type;
	auto& [lenght, width] = type;
	lenght = m_lights.size();
	width = m_lights[0].size();
	return type;
}

void Board::SetColorForLightOn(const Light::Color& color)
{
	m_colorLightOn = color;
}

void Board::SetColorForLightOff(const Light::Color& color)
{
	m_colorLightOff = color;
}

Board& Board::UpdateLight(const Position& pos)
{
	const auto& [line, column] = pos;
	this->ElementAt({ line, column }).UpdateLight(m_colorLightOn, m_colorLightOff);
	CheckLightsOn(pos);
	return *this;
}

Board& Board::UpdateUpLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line > 0)
	{
		this->ElementAt({ line - 1, column }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line - 1, column });
	}
	return*this;
}

Board& Board::UpdateDownLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line < m_lights.size() - 1)
	{
		this->ElementAt({ line + 1, column }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line + 1, column });
	}
	return*this;
}

Board& Board::UpdateLeftLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (column > 0)
	{
		this->ElementAt({ line , column - 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line, column - 1 });
	}
	return*this;
}

Board& Board::UpdateRightLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (column < m_lights[line].size() - 1)
	{
		this->ElementAt({ line , column + 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line, column + 1 });
	}
	return*this;
}

Board& Board::UpdateDiagonalUpRightLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line > 0 && column < m_lights[line].size() - 1)
	{
		this->ElementAt({ line - 1 , column + 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line - 1, column + 1 });
	}
	return*this;
}

Board& Board::UpdateDiagonalUpLeftLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line > 0 && column > 0)
	{
		this->ElementAt({ line - 1 , column - 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line - 1, column - 1 });
	}
	return*this;
}

Board& Board::UpdateDiagonalDownRightLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line < m_lights.size() - 1 && column < m_lights[line].size() - 1)
	{
		this->ElementAt({ line + 1 , column + 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line + 1, column + 1 });
	}
	return*this;
}

Board& Board::UpdateDiagonalDownLeftLight(const Position& pos)
{
	const auto& [line, column] = pos;
	if (line < m_lights.size() - 1 && column > 0)
	{
		this->ElementAt({ line + 1 , column - 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn({ line + 1, column - 1 });
	}
	return*this;
}

std::vector<std::vector<Light>> Board::GetLights()
{
	return m_lights;
}

void Board::EmptyBoard(const Type& type)
{
	auto& [line, column] = type;
	for (int indexLine = 0; indexLine < line; ++indexLine)
	{
		std::vector<Light> aux;
		for (int indexColumn = 0; indexColumn < column; ++indexColumn)
			aux.push_back(Light(m_colorLightOff, Light::State::OFF));
		m_lights.push_back(aux);
	}
}

bool Board::Finish()
{
	if (m_lightsOn.size() == 0)
	{
		return true;
	}
	return false;
}

void Board::DeleteBoard()
{
	m_lights.clear();
	m_lightsOn.clear();
}

void Board::SetTimeToResolveBoard(const double& time)
{
	m_timeToResolveBoard = time;
}

double& Board::GetTimeToResolveBoard()
{
	return m_timeToResolveBoard;
}

void Board::CheckLightsOn(const Board::Position& pos)
{
	const auto& [line, column] = pos;
	if (m_lights[line][column].GetState() == Light::State::ON)
		m_lightsOn.emplace(pos, m_lights[line][column]);
	else
		m_lightsOn.erase(pos);
}

std::istream& operator>>(std::istream& is, Board& board)
{
	
	Board::Position position;
	auto& [line, column] = position;
	double time;
	is >> time;
	board.SetTimeToResolveBoard(time);
	uint32_t width, length;
	is >> width >> length;
	for (line = 0; line < length; ++line)
	{
		board.m_lights.push_back(std::vector<Light>(width));
		for (column = 0; column < width; ++column)
		{
			bool aux;
			is >> aux;
			if (aux)
			{
				Light light(board.m_colorLightOff, Light::State::OFF);
				board.ElementAt(position) = light;
			}
			else
			{

				Light light(board.m_colorLightOn, Light::State::ON);
				board.ElementAt(position) = light;
				board.m_lightsOn.emplace(position, light);
			}
		}
	}
	return is;
}

std::ostream& operator<<(std::ostream& os, const Board& board)
{
	Board::Position position;
	auto& [line, column] = position;

	for (line = 0; line < board.m_lights.size(); ++line)
	{
		for (column = 0; column < board.m_lights[line].size(); ++column)
		{
			os << board.GetElementAt(position);
		}
		os << std::endl;
	}

	return os;
}
