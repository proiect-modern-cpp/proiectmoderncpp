#pragma once
#include"Board.h"
#include <iostream>
#include <string>
#include<fstream>

class Player
{
public:
	Player() = default;
	Player(const std::string&);
	~Player() = default;

	Board::Position SelectLight(Board&);
	void SelectColors(Board&);
	uint32_t SelectGameMode();
	uint32_t SelectGameSubmode();
	bool SelectFastMode();
	void ReadName();
	void ReadDimensions(uint32_t& lenght,uint32_t& width);
	uint32_t CheckInput(const std::string&);


	friend std::ostream& operator<<(std::ostream&, const Player&);

private:
	std::string m_name;
	static const uint32_t kFirstOption = 1, kSecondOption = 2;
};
