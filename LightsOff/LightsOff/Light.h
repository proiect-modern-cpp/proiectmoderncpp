#pragma once
#include <iostream>

class Light
{
public:
	enum class Color : uint8_t
	{
		RED,
		BLUE,
		GREEN,
		YELLOW,
		CYAN,
		MAGENTA
	};
	enum class State : uint8_t
	{
		ON,
		OFF
	};
	Light() = default;
	~Light() = default;
	Light(const Color&, const State&);

	Light& UpdateLight(const Color&, const Color&);

	Color GetColor()const;
	State GetState()const;
	friend std::ostream& operator<<(std::ostream&, const Light&);
private:
	Color m_color;
	State m_state : 1;
};

