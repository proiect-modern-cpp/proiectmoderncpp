#pragma once
#include <sstream>
#include <iostream>
#include <array>

template<class B, class L>
class GameUtilities
{
public:
	static void ClassicGame(B& board, const std::pair<uint32_t, uint32_t>& pos);
	static void LeftRightGame(B& board, const std::pair<uint32_t, uint32_t>& pos);
	static void UpDownGame(B& board, const std::pair<uint32_t, uint32_t>& pos);
	static void DiagonalUpRightDownLeftGame(B& board, const std::pair<uint32_t, uint32_t>& pos);
	static void DiagonalUpLeftDownRightGame(B& board, const std::pair<uint32_t, uint32_t>& pos);
	static void UpDownLeftRightAllDiagonalsGame(B& board, const std::pair<uint32_t, uint32_t>& pos);

	static bool SolvableBoard(B board);
	static void CreateUndefinedBoard(B&, const std::pair<uint32_t, uint32_t>& type, uint32_t);

	static void GetBoard(B&, const std::pair<uint32_t, uint32_t>& type);

	static void ApplyRule(B&, uint32_t, const std::pair<uint32_t, uint32_t>& pos);
	static void MenuGameModes();

	static bool IsPredefinedType(const std::pair<uint32_t, uint32_t>&);
	static std::pair<uint32_t, uint32_t> GetPredefinedType(uint32_t);

	static std::string GetGameMode(uint32_t);

	static const uint32_t kClassicGameMode = 1, kUpDownGameMode = 2, kLeftRightGameMode = 3,
		kDiagonalUpRightDownLeftGameMode = 4, kDiagonalUpLeftDownRightGameMode = 5, kUpDownLeftRightAllDiagonalsGameMode = 6;

private:

	static const uint32_t kNoMaxOfFiles = 5;
	static const uint16_t kMinType = 3, kMaxType = 9;
	static constexpr std::pair<uint32_t, uint32_t> A5 = { 4, 0 }, B5 = { 4, 1 }, B1 = { 0, 1 }, C5 = { 4, 2 }, D1 = { 0, 3 }, E1 = { 0,4 };
	inline static const std::vector<std::pair<uint32_t, uint32_t>> kPredefinedTypes{ {3,3}, {5,5}, {7,7}, {9,9} };
};

template<class B, class L>
inline void GameUtilities<B, L>::ClassicGame(B& board, const std::pair<uint32_t, uint32_t>& pos)
{
	board.UpdateLight(pos);
	board.UpdateUpLight(pos);
	board.UpdateDownLight(pos);
	board.UpdateLeftLight(pos);
	board.UpdateRightLight(pos);
}

template<class B, class L>
inline void GameUtilities<B, L>::LeftRightGame(B& board, const std::pair<uint32_t, uint32_t>& pos)
{
	board.UpdateLight(pos);
	board.UpdateLeftLight(pos);
	board.UpdateRightLight(pos);
}

template<class B, class L>
inline void GameUtilities<B, L>::UpDownGame(B& board, const std::pair<uint32_t, uint32_t>& pos)
{
	board.UpdateLight(pos);
	board.UpdateUpLight(pos);
	board.UpdateDownLight(pos);
}

template<class B, class L>
inline void GameUtilities<B, L>::DiagonalUpRightDownLeftGame(B& board, const std::pair<uint32_t, uint32_t>& pos)
{
	board.UpdateLight(pos);
	board.UpdateDiagonalUpRightLight(pos);
	board.UpdateDiagonalDownLeftLight(pos);
}

template<class B, class L>
inline void GameUtilities<B, L>::DiagonalUpLeftDownRightGame(B& board, const std::pair<uint32_t, uint32_t>& pos)
{
	board.UpdateLight(pos);
	board.UpdateDiagonalUpLeftLight(pos);
	board.UpdateDiagonalDownRightLight(pos);
}

template<class B, class L>
inline void GameUtilities<B, L>::UpDownLeftRightAllDiagonalsGame(B& board, const std::pair<uint32_t, uint32_t>& pos)
{
	board.UpdateLight(pos);
	board.UpdateUpLight(pos);
	board.UpdateDownLight(pos);
	board.UpdateLeftLight(pos);
	board.UpdateRightLight(pos);
	board.UpdateDiagonalUpRightLight(pos);
	board.UpdateDiagonalDownLeftLight(pos);
	board.UpdateDiagonalUpLeftLight(pos);
	board.UpdateDiagonalDownRightLight(pos);
}

template<class B, class L>
inline bool GameUtilities<B, L>::SolvableBoard(B board)
{
	std::pair<uint32_t, uint32_t> position;
	for (int line = 0; line < board.GetLights().size() - 1; ++line)
		for (int column = 0; column < board.GetLights().size(); ++column)
		{
			if (board.GetLights()[line][column].GetState() == L::State::ON)
			{
				position = { line + 1,column };
				GameUtilities::ClassicGame(board, position);
			}
		}
	if (board.GetElementAt(A5).GetState() == L::State::ON)
	{
		GameUtilities::ClassicGame(board, E1);
		GameUtilities::ClassicGame(board, D1);
		if (SolvableBoard(board))
			return true;
	}
	if (board.GetElementAt(B5).GetState() == L::State::ON)
	{
		GameUtilities::ClassicGame(board, E1);
		GameUtilities::ClassicGame(board, B1);
		if (SolvableBoard(board))
			return true;
	}
	if (board.GetElementAt(C5).GetState() == L::State::ON)
	{
		GameUtilities::ClassicGame(board, D1);
		if (SolvableBoard(board))
			return true;
	}
	if (board.Finish())
		return true;
	return false;
}

template<class B, class L>
inline void GameUtilities<B, L>::CreateUndefinedBoard(B& board, const std::pair<uint32_t, uint32_t>& type, uint32_t option)
{
	board.DeleteBoard();
	board.EmptyBoard(type);
	auto& [line, column] = type;
	int noLights = rand() % (type.first + type.second) + 4;
	while (noLights) {
		int indexLine = rand() % line;
		int indexColumn = rand() % column;
		std::pair<uint32_t, uint32_t> pos;
		pos = { indexLine,indexColumn };
		GameUtilities::ApplyRule(board, option, pos);
		noLights--;
	}
}

template<class B, class L>
inline void GameUtilities<B, L>::GetBoard(B& board, const std::pair<uint32_t, uint32_t>& type)
{
	std::string fileName = "../Resources/Boards/";
	std::stringstream stringStream;
	stringStream << std::to_string(type.first) << "x" << std::to_string(type.second) << ".";
	uint32_t index = rand() % kNoMaxOfFiles + 1;
	stringStream << std::to_string(index) << ".txt";
	fileName.append(stringStream.str());
	std::ifstream inputFile(fileName);
	inputFile >> board;
	inputFile.close();
}

template<class B, class L>
inline void GameUtilities<B, L>::ApplyRule(B& board, uint32_t option, const std::pair<uint32_t, uint32_t>& pos)
{
	switch (option)
	{
	case kClassicGameMode: GameUtilities<B, L>::ClassicGame(board, pos); break;
	case kUpDownGameMode: GameUtilities<B, L>::UpDownGame(board, pos); break;
	case kLeftRightGameMode: GameUtilities<B, L>::LeftRightGame(board, pos); break;
	case kDiagonalUpRightDownLeftGameMode: GameUtilities<B, L>::DiagonalUpRightDownLeftGame(board, pos); break;
	case kDiagonalUpLeftDownRightGameMode: GameUtilities<B, L>::DiagonalUpLeftDownRightGame(board, pos); break;
	case kUpDownLeftRightAllDiagonalsGameMode: GameUtilities<B, L>::UpDownLeftRightAllDiagonalsGame(board, pos); break;
	default:
		break;
	}
}

template<class B, class L>
inline void GameUtilities<B, L>::MenuGameModes()
{
	system("cls");
	std::cout << "Game modes: \n";
	std::cout << "1 -> Classic (up-down-left-right) \n";
	std::cout << "2 -> Up-down \n";
	std::cout << "3 -> Left-right \n";
	std::cout << "4 -> Upper right diagonal-bottom left diagonal \n";
	std::cout << "5 -> Upper left diagonal-bottom right diagonal \n";
	std::cout << "6 -> Up-down-left-right-diagonal \n";
}

template<class B, class L>
inline bool GameUtilities<B, L>::IsPredefinedType(const std::pair<uint32_t, uint32_t>& boardType)
{
	for (auto type : GameUtilities<B, L>::kPredefinedTypes)
		if (type == boardType)
			return true;
	return false;
}

template<class B, class L>
inline std::pair<uint32_t, uint32_t> GameUtilities<B, L>::GetPredefinedType(uint32_t index)
{
	return  kPredefinedTypes[index];
}

template<class B, class L>
inline std::string GameUtilities<B, L>::GetGameMode(uint32_t option)
{
	switch (option)
	{
	case kClassicGameMode: return " up-down-left-right neighbors"; break;
	case kUpDownGameMode: return " up-down neighbors"; break;
	case kLeftRightGameMode:return " left-right game"; break;
	case kDiagonalUpRightDownLeftGameMode: return " diagonal up right-diagonal down left neighbors"; break;
	case kDiagonalUpLeftDownRightGameMode: return " diagonal up left-diagonal down right neighbors"; break;
	case kUpDownLeftRightAllDiagonalsGameMode: return " up-down-left-right and diagonals neighbors"; break;
	default:
		break;
	}

}
