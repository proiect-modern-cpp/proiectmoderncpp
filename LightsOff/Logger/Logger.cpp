#include "Logger.h"
#include <iomanip>
#include <fstream>
#include <ctime>
#include <chrono>

const char* LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}



Logger::Logger(std::ostream& os, Logger::Level minimumLevel) :
	m_os{ os },
	m_minimumLevel{ minimumLevel }
{
	// Empty
}

void Logger::setMinLevel(Level minimumLevel)
{
	m_minimumLevel = minimumLevel;
}

void Logger::log(const char* message, Level level)
{
	if (static_cast<int>(level) < static_cast<int>(m_minimumLevel))
		return;
	std::time_t now = time(nullptr);
	char localTimeAndDate[kBufferSize];
	ctime_s(localTimeAndDate, kBufferSize, &now);
	m_os << "[" << LogLevelToString(level) << ']' << '[';
	for (int index = 0; index < kBufferSize - 2; ++index)
		m_os << localTimeAndDate[index];
	m_os << ']';
	m_os << message << std::endl;
}

void Logger::log(const std::string& message, Level level)
{
	this->log(message.c_str(), level);
}

void Logger::setMinimumLogLevel(Level level)
{
	this->m_minimumLevel = level;
}
