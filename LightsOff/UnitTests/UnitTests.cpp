#include "pch.h"
#include "CppUnitTest.h"
#include <sstream>
#include <iomanip>
#include "../LightsOff/Light.h"
#include "../LightsOff/Light.cpp"
#include "../LightsOff/Board.h"
#include "../LightsOff/Board.cpp"
#include "../LightsOff/Player.h"
#include "../LightsOff/Player.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(LightTests)
	{
	public:
		TEST_METHOD(LightConstructor)
		{
			Light light(Light::Color::BLUE, Light::State::ON);
			Assert::IsTrue(light.GetColor() == Light::Color::BLUE);
			Assert::IsTrue(light.GetState() == Light::State::ON);
		}
		TEST_METHOD(UpdateLightFunction)
		{
			Light light(Light::Color::BLUE, Light::State::ON);

			light.UpdateLight(Light::Color::BLUE, Light::Color::RED);

			Assert::IsTrue(light.GetColor() == Light::Color::RED);
			Assert::IsTrue(light.GetState() == Light::State::OFF);
		}
		TEST_METHOD(GetColorLight)
		{
			Light light(Light::Color::BLUE, Light::State::OFF);

			Light::Color color = light.GetColor();

			Assert::IsTrue(color == Light::Color::BLUE);
			
		}
		TEST_METHOD(GetStateLight)
		{
			Light light(Light::Color::BLUE, Light::State::OFF);

			Light::State state = light.GetState();

			Assert::IsTrue(state == Light::State::OFF);
		}
	};
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(UpdateUpLightFunction)
		{
			Board board;
			board.EmptyBoard({ 3,3 });
			board.UpdateUpLight({ 1,0 });

			Light light = board.GetElementAt({ 0,0 });

			Assert::IsTrue(light.GetState() == Light::State::ON);
		}

		TEST_METHOD(UpdateDownLightFunction)
		{
			Board board;
			board.EmptyBoard({ 3,3 });
			board.UpdateDownLight({ 0,0 });

			Light light = board.GetElementAt({ 1,0 });

			Assert::IsTrue(light.GetState() == Light::State::ON);
		}

		TEST_METHOD(UpdateLeftLightFunction)
		{
			Board board;
			board.EmptyBoard({ 3,3 });
			board.UpdateLeftLight({ 1,1 });

			Light light = board.GetElementAt({ 1,0 });

			Assert::IsTrue(light.GetState() == Light::State::ON);
		}

		TEST_METHOD(UpdateRightLightFunction)
		{
			Board board;
			board.EmptyBoard({ 3,3 });
			board.UpdateRightLight({ 1,0 });

			Light light = board.GetElementAt({ 1,1 });

			Assert::IsTrue(light.GetState() == Light::State::ON);
		}

		TEST_METHOD(UpdateDiagonalUpRightLightFunction)
		{
			Board board;
			board.EmptyBoard({ 3,3 });
			board.UpdateDiagonalUpRightLight({ 1,1 });

			Light light = board.GetElementAt({ 0,2 });

			Assert::IsTrue(light.GetState() == Light::State::ON);
		}

		TEST_METHOD(UpdateDiagonalUpLeftLightFunction)
		{
			Board board;
			board.EmptyBoard({ 3,3 });
			board.UpdateDiagonalUpLeftLight({ 1,1 });

			Light light = board.GetElementAt({ 0,0 });

			Assert::IsTrue(light.GetState() == Light::State::ON);
		}

		TEST_METHOD(EmptyBoard)
		{
			Board board;
			board.EmptyBoard({ 4,5 });

			int line = rand() % board.GetLights().size();
			int column = rand() % board.GetLights()[line].size();

			Assert::IsTrue(board.GetLights().size() == 4);
			Assert::IsTrue(board.GetLights()[line].size() == 5);
			Assert::IsTrue(board.ElementAt(Board::Position(line,column)).GetState()==Light::State::OFF);
		}

		TEST_METHOD(DeleteBoard)
		{
			Board board;
			board.EmptyBoard({ 4,5 });
			board.DeleteBoard();
			Assert::IsTrue(board.GetLights().size() == 0);
		}

		TEST_METHOD(GetType)
		{
			Board board;
			board.EmptyBoard({ 4,5 });
			Assert::IsTrue(board.GetType() == Board::Type{4, 5});
		}

		TEST_METHOD(GetTimeToResolveBoard)
		{
			Board board;
			double time = 1.234;
			board.SetTimeToResolveBoard(time);
			Assert::IsTrue(board.GetTimeToResolveBoard() == time);
		}

		TEST_METHOD(Finish)
		{
			Board board;
			board.EmptyBoard({ 4,5 });
			Assert::IsTrue(board.Finish() == true);
		}
		TEST_METHOD(SetColorForLightOn)
		{
			Light::Color color = Light::Color::RED;
			Board board;
			board.SetColorForLightOn(color);
			board.EmptyBoard({ 3,5 });
			board.UpdateLight({ 1,1 });
			Assert::IsTrue(board.GetElementAt({ 1,1 }).GetColor() == Light::Color::RED);
		}
		TEST_METHOD(SetColorForLightOff)
		{
			Light::Color color = Light::Color::RED;
			Board board;
			board.SetColorForLightOff(color);
			board.EmptyBoard({ 3,5 });
			Assert::IsTrue(board.GetElementAt({ 1,1 }).GetColor() == Light::Color::RED);
		}
		TEST_METHOD(GetElementAt)
		{
			Board board;
			board.EmptyBoard({ 4,5 });
			Assert::IsTrue(board.GetElementAt({ 1,1 }).GetState() == Light::State::OFF);
		}
		TEST_METHOD(GetLights)
		{
			Board board;
			board.SetColorForLightOff(Light::Color::GREEN);
			board.SetColorForLightOn(Light::Color::RED);
			board.EmptyBoard({ 2,2 });
			board.UpdateLight({ 1,1 });
			std::vector<std::vector<Light>> lights;
			std::vector<Light>auxLights;
			auxLights.push_back({ Light::Color::GREEN,Light::State::OFF });
			auxLights.push_back({ Light::Color::GREEN,Light::State::OFF });
			lights.push_back(auxLights);
			auxLights.clear();
			auxLights.push_back({ Light::Color::GREEN,Light::State::OFF });
			auxLights.push_back({ Light::Color::RED,Light::State::ON });
			lights.push_back(auxLights);
			std::vector<std::vector<Light>> boardLights = board.GetLights();
			bool verif = true;
			if (boardLights.size() != lights.size())
				verif = false;
			for(int index1=0; index1<boardLights.size();index1++)
				for (int index2 = 0; index2 < boardLights[index1].size(); index2++)
				{
					if (boardLights[index1].size() != lights[index1].size())
					{
						verif = false;
						break;
					}
					if (boardLights[index1][index2].GetColor() != lights[index1][index2].GetColor() || boardLights[index1][index2].GetState() != lights[index1][index2].GetState())
					{
						verif = false;
						break;
					}
				}
			Assert::IsTrue(verif == true);
		}

	};

	TEST_CLASS(PlayerTests)
	{
	public:
		TEST_METHOD(PlayerConstructor)
		{
			Player player("name");
			std::stringstream ss;
			ss << player;
			Assert::IsTrue(ss.str() == "name");
		}
		
	};

}
