#include "Light.h"

namespace gui {

	Light::Light(const sf::Color& color, const State& state)
		:m_color(color), m_state(state)
	{
		this->setRadius(kdefaultDiameter);
	}

	Light& Light::UpdateLight(const sf::Color& on, const sf::Color& off)
	{

		if (m_state == Light::State::ON)
		{
			this->setFillColor(off);
			m_state = Light::State::OFF;
		}
		else
		{
			this->setFillColor(on);
			m_state = Light::State::ON;
		}
		return *this;
	}

	sf::Color Light::GetColor() const
	{
		return m_color;
	}

	Light::State Light::GetState() const
	{
		return m_state;
	}

	bool Light::operator==(const Light& light)
	{
		if (this->getFillColor() == light.getFillColor() && this->m_state == light.GetState() && this->getPosition() == light.getPosition())
			return true;
		return false;
	}

	bool Light::operator<(const Light& light)const
	{
		if (this->getPosition().x < light.getPosition().x)
			return true;
		if (this->getPosition().y < light.getPosition().y)
			return true;
		return false;
	}

	void Light::SetDiameter(int length, int width)
	{
		if (length >= kSizeBoard || width >= kSizeBoard)
		{
			if (length > width)
				this->setRadius(kdefaultDiameter - length);
			else
				this->setRadius(kdefaultDiameter - width);
		}
	}

}