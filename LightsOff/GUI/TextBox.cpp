#include "TextBox.h"

void TextBox::VerifyInput(int charTyped)
{
	if (charTyped != '\b' && charTyped != sf::Keyboard::Enter && charTyped != sf::Keyboard::Escape)
	{
		m_stream << static_cast<char>(charTyped);
	}
	else if (charTyped == '\b')
	{
		if (m_stream.str().length() > 0)
		{
			DeleteLastChar();
		}
	}
	m_textBox.setString(m_stream.str() + "_");
}

void TextBox::DeleteLastChar()
{
	std::string aux = m_stream.str();
	std::string newAux = "";

	for (int index = 0; index < aux.length() - 1; ++index)
		newAux += aux[index];

	m_stream.str("");
	m_stream << newAux;
	m_textBox.setString(m_stream.str());
}

TextBox::TextBox(int characterSize, const sf::Color& color, const sf::Vector2f& pos, int BoxLenght, int BoxWidth)
{
	m_textBox.setCharacterSize(characterSize);
	
	if(!m_font.loadFromFile("../Resources/Virgo.ttf"))
		throw std::runtime_error("Failed to load Virgo.ttf");
	m_textBox.setFont(m_font);
	m_textBox.setFillColor(color);
	m_textBox.setPosition(pos);
	m_box.setPosition(pos.x - 10, pos.y - 10);
	m_isPressed = false;
	if (m_isPressed)
		m_textBox.setString("_");
	else
		m_textBox.setString("");

	m_box.setOutlineColor(sf::Color::Magenta);
	m_box.setOutlineThickness(2);
	m_box.setFillColor(sf::Color::Transparent);

	m_box.setSize({ static_cast<float>(BoxLenght), static_cast<float>(BoxWidth) });
}

void TextBox::SetPressed(bool isPressed)
{
	m_isPressed = isPressed;
	if (!isPressed)
	{
		m_textBox.setString(m_stream.str());
	}
}

void TextBox::SetLimit(bool hasLimit, int limit)
{
	m_hasLimit = hasLimit;
	m_limit = limit;
}

std::string TextBox::GetText() const
{
	return m_stream.str();
}

bool TextBox::GetPressed() const
{
	return m_isPressed;
}

void TextBox::Draw(std::shared_ptr<sf::RenderWindow> window) const
{
	window->draw(m_box);
	window->draw(m_textBox);
}

void TextBox::WriteOnBox(sf::Event input)
{
	if (m_isPressed)
	{
		int charTyped = input.text.unicode;
		if (charTyped < 128)
		{
			if (m_hasLimit)
			{
				if (m_stream.str().length() <= m_limit)
					VerifyInput(charTyped);
				else if (m_stream.str().length() > m_limit && charTyped == '\b')
				{
					DeleteLastChar();
					m_textBox.setString(m_stream.str() + "_");
				}
			}
			else
				VerifyInput(charTyped);
		}
	}
}

bool TextBox::IsPressed(const sf::Vector2f& pos) const
{
	sf::FloatRect bounds = m_box.getGlobalBounds();
	if (bounds.contains(pos))
		return true;
	return false;
}