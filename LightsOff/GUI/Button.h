#pragma once
#include "SFML/Graphics.hpp"
#include <string>
#include <functional>

class Button {
public:
    Button(std::string text = "", std::function<void(void)> callback = nullptr);
    Button(float, float, float, float, sf::Color color, std::string text = "", std::function<void(void)> callback = nullptr);

    void Select();
    void SetText(const std::string&);
    const std::string& GetText() const;
    void SetCallback(std::function<void(void)>);

    void Update(const sf::Event&);
    void Draw(std::shared_ptr<sf::RenderTarget>) const;
    void SetTextSize(float);

    void SetPosition(const sf::Vector2f&);
    void SetColor(const sf::Color&);
    void SetSize(const sf::Vector2f&);

    void ConfigureButton(const sf::Vector2f&, const sf::Vector2f&, const std::string&, const sf::Color&);

	void ResetPressed();

    bool IsPressed();
    sf::RectangleShape GetShape();

private:
    std::string m_textString;
    std::function<void(void)> m_callback;

    sf::RectangleShape m_shape;
    sf::Font m_font;
    sf::Text m_text;
    sf::Color m_color;
    bool m_isPressed;

    bool isButtonHover(sf::Vector2f);
};

