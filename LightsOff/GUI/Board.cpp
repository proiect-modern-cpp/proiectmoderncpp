#include "Board.h"
#include <iostream>

namespace gui {

	std::istream& operator>>(std::istream& is, Board& board)
	{
		double time;
		is >> time;
		board.SetTimeToResolveBoard(time);
		uint32_t width, length;
		is >> width >> length;
		for (uint32_t line = 0; line < length; ++line)
		{

			board.m_lights.push_back(std::vector<Light>(width));
			for (uint32_t column = 0; column < width; ++column)
			{
				bool aux;
				is >> aux;
				if (aux)
				{
					Light light(board.m_colorLightOff, Light::State::OFF);
					light.setFillColor(board.m_colorLightOff);
					light.SetDiameter(length, width);
					board.GetElementAt({ line, column }) = std::move(light);
				}
				else
				{

					Light light(board.m_colorLightOn, Light::State::ON);
					light.setFillColor(board.m_colorLightOn);
					light.SetDiameter(length, width);
					board.m_CountLightsOn++;
					board.GetElementAt({ line, column }) = std::move(light);
				}
			}
		}
		return is;
	}


	Board::Board()
	{
		m_CountLightsOn = 0;
		SetTimeToResolveBoard();
	}

	void Board::SetColorForLightOn(const sf::Color& color)
	{
		m_colorLightOn = color;
	}

	void Board::SetColorForLightOff(const sf::Color& color)
	{
		m_colorLightOff = color;
	}

	Light& Board::GetElementAt(std::pair<uint32_t, uint32_t> position)
	{
		auto& [line, column] = position;
		return m_lights[line][column];
	}

	std::vector<std::vector<Light>> Board::GetLights()
	{
		return m_lights;
	}

	void Board::SetTimeToResolveBoard(const double& time)
	{
		m_timeToResolveBoard = time;
	}

	Board& Board::UpdateLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		this->GetElementAt({ line, column }).UpdateLight(m_colorLightOn, m_colorLightOff);
		CheckLightsOn(pos);
		return *this;
	}

	Board& Board::UpdateUpLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (line > 0)
		{
			this->GetElementAt({ line - 1, column }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line - 1, column });
		}
		return*this;
	}

	Board& Board::UpdateDownLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (line < m_lights.size() - 1)
		{
			this->GetElementAt({ line + 1, column }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line + 1, column });
		}
		return*this;
	}

	Board& Board::UpdateLeftLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (column > 0)
		{
			this->GetElementAt({ line, column - 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line, column - 1 });
		}
		return*this;
	}

	Board& Board::UpdateRightLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (column < m_lights[line].size() - 1)
		{
			this->GetElementAt({ line, column + 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line, column + 1 });
		}
		return*this;
	}

	Board& Board::UpdateDiagonalUpRightLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (line > 0 && column < m_lights[line].size() - 1)
		{
			this->GetElementAt({ line - 1, column + 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line - 1, column + 1 });
		}
		return*this;
	}

	Board& Board::UpdateDiagonalUpLeftLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (line > 0 && column > 0)
		{
			this->GetElementAt({ line - 1, column - 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line - 1, column - 1 });
		}
		return*this;
	}

	Board& Board::UpdateDiagonalDownRightLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (line < m_lights.size() - 1 && column < m_lights[line].size() - 1)
		{
			this->GetElementAt({ line + 1, column + 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line + 1, column + 1 });
		}
		return*this;
	}

	Board& Board::UpdateDiagonalDownLeftLight(const Position& pos)
	{
		const auto& [line, column] = pos;
		if (line < m_lights.size() - 1 && column > 0)
		{
			this->GetElementAt({ line + 1, column - 1 }).UpdateLight(m_colorLightOn, m_colorLightOff);
			CheckLightsOn({ line + 1, column - 1 });
		}
		return*this;
	}

	void Board::EmptyBoard(const Type& type)
	{
		for (int indexLine = 0; indexLine < type.first; ++indexLine)
		{
			std::vector<Light> aux;
			for (int indexColumn = 0; indexColumn < type.second; ++indexColumn)
			{
				Light light(m_colorLightOff, Light::State::OFF);
				light.setFillColor(m_colorLightOff);
				light.SetDiameter(type.first, type.second);
				aux.push_back(light);
			}
			m_lights.push_back(aux);
		}
	}

	bool Board::Finish()
	{
		if (m_CountLightsOn)
			return false;
		return true;
	}

	void Board::DeleteBoard()
	{
		m_lights.clear();
		m_CountLightsOn = 0;
	}

	double& Board::GetTimeToResolveBoard()
	{
		return m_timeToResolveBoard;
	}

	void Board::CheckLightsOn(const std::pair<uint32_t, uint32_t>& pos)
	{
		const auto& [line, column] = pos;
		if (m_lights[line][column].GetState() == Light::State::ON)
			m_CountLightsOn++;
		else
			m_CountLightsOn--;
	}

	void Board::PositionOnWindow(const sf::Vector2u& windowSize)
	{
		sf::Vector2f orig;

		for (int line = 0; line < m_lights.size(); ++line)
		{
			for (int column = 0; column < m_lights[line].size(); ++column)
			{
				orig.x = windowSize.x * 0.5 - this->m_lights[0].size() * 2.5 * m_lights[line][column].getRadius() / 2 + 2.5 * column * m_lights[line][column].getRadius();
				orig.y = windowSize.y * 0.5 - this->m_lights.size() * 2.5 * m_lights[line][column].getRadius() / 2 + 2.5 * line * m_lights[line][column].getRadius();
				m_lights[line][column].setPosition(orig.x, orig.y);
			}
		}
	}

	void Board::draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		sf::RectangleShape outline;
		outline.setSize(sf::Vector2f(m_lights[0].size() * m_lights[0][0].getRadius() * 2.5 + 50, m_lights.size() * m_lights[0][0].getRadius() * 2.5 + 50));
		outline.setFillColor(sf::Color(0, 0, 0, 130));
		outline.setPosition(m_lights[0][0].getPosition().x - m_lights[0][0].getRadius(), m_lights[0][0].getPosition().y - m_lights[0][0].getRadius());
		target.draw(outline);
		for (int line = 0; line < m_lights.size(); ++line)
		{
			for (int column = 0; column < m_lights[line].size(); ++column)
			{
				target.draw(m_lights[line][column]);
			}
		}
	}
}