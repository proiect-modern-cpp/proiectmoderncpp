#pragma once
#include <SFML/Graphics.hpp>
#include <fstream>
#include "Game.h"
#include "Button.h"
#include "Menu.h"
#include "../Logger/Logger.h"

int main()
{
	Logger logger(std::cout);
	logger.log("Application started.", Logger::Level::Info);

	srand(time(0));
	std::shared_ptr<sf::RenderWindow> window = std::make_shared<sf::RenderWindow>(sf::VideoMode(1400, 800), "Lights Off", sf::Style::Titlebar | sf::Style::Close);
	try
	{
		Menu mainmenu;
		gui::Game game(window);
		Button start("Start", [&game]() { game.Run(); });
		Button exit("Exit", [&window]() { window->close(); });
		mainmenu.AddOption(start);
		mainmenu.AddOption(exit);
		mainmenu.AddTitle("Lights Off");
		mainmenu.SetLayout(MenuLayout::VerticleCentered);
		mainmenu.Display(window);
	}
	catch (const std::runtime_error& error)
	{
		logger.log(error.what(), Logger::Level::Error);
	}

	logger.log("Application stopped.", Logger::Level::Info);
}