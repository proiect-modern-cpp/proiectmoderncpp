#pragma once

#include <vector>
#include <string>
#include <functional>
#include "Button.h"
#include <SFML/Graphics.hpp>

class MenuLayout {
	friend class Menu;
public:
	MenuLayout(unsigned int spacing = 50, const sf::Vector2f& origin = sf::Vector2f(0.5, 0.5));
	MenuLayout(const MenuLayout&);
	void SetSpacing(unsigned int);
	void SetOrigin(const sf::Vector2f&);

	static const MenuLayout VerticleCentered;
	static const MenuLayout VerticleLeft;
	static const MenuLayout VerticleRight;
private:
	void Apply(std::vector<sf::Text>&, const sf::Vector2u&);
	unsigned int m_spacing;
	sf::Vector2f m_origin;
};

class Menu {
public:
	Menu();
	void Display(std::shared_ptr<sf::RenderWindow>);
	void AddOption(const Button&);
	void AddTitle(const std::string&);
	void ClearOptions();
	void SetBackground(const std::string&);
	void SetExitCallback(std::function<void(void)> callback);
	void SetLayout(const MenuLayout&);
	void SetTemplateText(const sf::Text&);
private:
	std::vector<Button> m_options;
	sf::Texture m_background;
	std::function<void(void)> m_exitCallback;
	MenuLayout m_layout;
	sf::Text m_templateText;
	sf::Font m_font;
};

