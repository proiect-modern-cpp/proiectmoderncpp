#pragma once
#include <SFML/Graphics.hpp>
#include "Board.h"
#include "../../LightsOff/GameUtilities.h"
#include "Player.h"
#include "../Logger/Logger.h"
#include <map>
#include <array>

namespace gui {
	class Game
	{
	private:
		Board m_board;
		Button m_changeBoard, m_resetBoard, m_reconfigure, m_changePlayer;

		std::shared_ptr<sf::RenderWindow> m_window;
		sf::Event m_event;

		Player m_player;

		//Mouse positions
		sf::Vector2i m_mousePosWindow;
		sf::Vector2f m_mousePosView;

		//Resources
		sf::Font m_font;
		sf::Texture m_backgroundTexture;
		sf::Sprite m_background;

		void GameLoop(uint32_t gameMode, const Board::Type& type, bool fastMode);

	public:
		Game(std::shared_ptr<sf::RenderWindow>);
		void Run(bool option = true);
		void ChangeBoard(uint32_t, const Board::Type&, bool);
		void GameLost(uint32_t, const Board::Type&, bool, const Board& aux);
		void GameWon(uint32_t, const Board::Type&, bool, const Board& aux);
		~Game() = default;
		static const uint32_t kStandardGameMode = 1;
		inline static const std::vector<std::pair<uint32_t, uint32_t>> kSadFace{ {1,1}, {1,5}, {2,3}, {4,2}, {4,3}, {5,1}, {4,4}, {5,5} };
	};
}