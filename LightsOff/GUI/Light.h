#pragma once
#include <SFML/Graphics.hpp>

namespace gui {
	class Light : public sf::CircleShape
	{
	public:
		enum class State : uint8_t
		{
			ON,
			OFF
		};


		Light() = default;
		Light(const sf::Color&, const State&);

		Light& UpdateLight(const sf::Color&, const sf::Color&);

		sf::Color GetColor()const;
		State GetState()const;

		bool operator==(const Light&);
		bool operator<(const Light&) const;

		void SetDiameter(int, int);

	private:
		sf::Color m_color;
		State m_state : 1;
		static const uint32_t kdefaultDiameter = 33;
		static const uint32_t kSizeBoard = 6;
	};
}