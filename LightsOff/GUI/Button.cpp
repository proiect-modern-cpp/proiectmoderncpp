#include "Button.h"

Button::Button(std::string text, std::function<void(void)> callback) :m_textString(text), m_callback(callback) { }

Button::Button(float x, float y, float width, float height, sf::Color color, std::string text, std::function<void(void)> callback) : m_callback(callback)
{
	this->m_shape.setPosition(sf::Vector2f(x, y));
	this->m_shape.setSize(sf::Vector2f(width, height));
	if(!this->m_font.loadFromFile("../Resources/Virgo.ttf"))
		throw std::runtime_error("Failed to load Virgo.ttf");

	this->m_text.setFont(this->m_font);
	this->m_text.setString(text);
	this->m_text.setFillColor(sf::Color::White);
	this->m_text.setCharacterSize(30);

	this->m_text.setPosition(
		this->m_shape.getPosition().x + 15,
		this->m_shape.getPosition().y + 15
	);
	this->m_color = color;
	this->m_shape.setFillColor(this->m_color);
	this->m_isPressed = false;
}

void Button::Select()
{
	if (m_callback)
		m_callback();
}

void Button::SetText(const std::string& text)
{
	if(!this->m_font.loadFromFile("../Resources/Virgo.ttf"))
		throw std::runtime_error("Failed to load Virgo.ttf");
	this->m_text.setFont(this->m_font);
	this->m_text.setString(text);
	this->m_text.setFillColor(sf::Color::White);
	this->m_text.setCharacterSize(30);
}

const std::string& Button::GetText() const
{
	return m_textString;
}

void Button::SetCallback(std::function<void(void)> callback)
{
	this->m_callback = callback;
}

void Button::Update(const sf::Event& event)
{
	if (this->isButtonHover(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)) == true)
		if (event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left)
		{
			Select();
			this->m_isPressed = true;
		}
}

void Button::Draw(std::shared_ptr<sf::RenderTarget> target) const
{
	target->draw(m_shape);
	target->draw(m_text);
}

void Button::SetTextSize(float size)
{
	m_text.setCharacterSize(size);
}

void Button::SetPosition(const sf::Vector2f& pos)
{
	m_shape.setPosition(pos);
	m_text.setPosition(
		m_shape.getPosition().x + 15,
		m_shape.getPosition().y + 15
	);
}

void Button::SetColor(const sf::Color& color)
{
	m_color = color;
	m_shape.setFillColor(m_color);
}

void Button::SetSize(const sf::Vector2f& size)
{
	m_shape.setSize(size);
}

void Button::ConfigureButton(const sf::Vector2f& pos, const sf::Vector2f& size, const std::string& text, const sf::Color& color)
{
	SetPosition(pos);
	SetSize(size);
	SetText(text);
	SetColor(color);
	m_isPressed = false;
}

void Button::ResetPressed()
{
	m_isPressed = false;
}

bool Button::IsPressed()
{
	return m_isPressed;
}

sf::RectangleShape Button::GetShape()
{
	return m_shape;
}

bool Button::isButtonHover(sf::Vector2f mousePosition)
{
	if (m_text.getGlobalBounds().contains(mousePosition))
		return true;
	return false;
}