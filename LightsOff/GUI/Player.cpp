#include "Player.h"
#include <regex>
#include <array>
#include <sstream>

namespace gui {
	Player::Player(sf::Text name) :m_name{ name } {}

	uint32_t Player::ChooseGameMode()
	{
		uint32_t option;

		Menu menu;

		menu.AddTitle("Choose how to play:");
		menu.SetLayout(MenuLayout::VerticleLeft);

		Button classic("up-down-left-right neighbors", [&option]() {option = 1; });
		menu.AddOption(classic);

		Button upDown("up-down neighbors", [&option]() { option = 2; });
		menu.AddOption(upDown);

		Button leftRight("left-right game", [&option]() { option = 3; });
		menu.AddOption(leftRight);

		Button diagonalUpRightDownLeft("diagonal up right-diagonal down left neighbors", [&option]() { option = 4; });
		menu.AddOption(diagonalUpRightDownLeft);

		Button diagonalUpLeftDownRight("diagonal up left-diagonal down right neighbors", [&option]() { option = 5; });
		menu.AddOption(diagonalUpLeftDownRight);

		Button upDownLeftRightAllDiagonals("up-down-left-right and diagonals neighbors", [&option]() { option = 6; });
		menu.AddOption(upDownLeftRightAllDiagonals);

		menu.Display(m_window);

		return option;
	}

	void Player::ChooseColors(Board& board)
	{
		sf::Sprite background;
		background.setTexture(m_background);
		background.setScale(m_window->getSize().x / (float)m_background.getSize().x, m_window->getSize().y / (float)m_background.getSize().y);

		sf::Text chooseOn("Choose color for lights on:", m_font, 30), chooseOff("Choose color for lights off:", m_font, 30);
		chooseOn.setFillColor(sf::Color::Magenta);
		chooseOn.setStyle(sf::Text::Bold | sf::Text::Italic);
		chooseOn.setPosition({ 10, 100 });

		chooseOff.setFillColor(sf::Color(10, 100, 140, 250));
		chooseOff.setStyle(sf::Text::Bold | sf::Text::Italic);
		chooseOff.setPosition({ 670, 100 });

		std::vector<Light> lightsOn, lightsOff;
		Light light(sf::Color::Red, Light::State::OFF);
		light.setFillColor(sf::Color::Red);
		lightsOn.push_back(light);
		light.setFillColor(sf::Color::Yellow);
		lightsOn.push_back(light);
		light.setFillColor(sf::Color::Green);
		lightsOn.push_back(light);
		light.setFillColor(sf::Color::Blue);
		lightsOn.push_back(light);
		light.setFillColor(sf::Color::Cyan);
		lightsOn.push_back(light);
		light.setFillColor(sf::Color::Magenta);
		lightsOn.push_back(light);

		lightsOff = lightsOn;

		float spacingBetweenLights = 2.5;
		bool on = false, off = false;

		while (m_window->isOpen())
		{
			sf::Event event;
			sf::Vector2f mousePosView = m_window.get()->mapPixelToCoords(sf::Mouse::getPosition(*m_window));
			while (m_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					m_window->close();
				else
				{
					if (event.type == sf::Event::MouseButtonPressed)
					{
						auto chooseColor = [this, &board, mousePosView](std::vector<Light>& availableColors, std::vector<Light>& colorsUpdated, bool& on, bool& off, bool setColorOn)
						{
							for (int index = 0; index < availableColors.size(); ++index)
							{
								if (availableColors[index].getGlobalBounds().contains(mousePosView) && on == false)
								{
									if (setColorOn == true)
										board.SetColorForLightOn(availableColors[index].getFillColor());
									else
										board.SetColorForLightOff(availableColors[index].getFillColor());
									availableColors.erase(availableColors.end() - availableColors.size() + index + 1, availableColors.end());
									availableColors.erase(availableColors.begin(), availableColors.begin() + index);

									if (off == false)
										colorsUpdated.erase(colorsUpdated.begin() + index);
									on = true;
								}
							}
						};
						chooseColor(lightsOn, lightsOff, on, off, true);
						chooseColor(lightsOff, lightsOn, off, on, false);
					}
				}
			}
			m_window->clear();
			m_window->draw(background);
			auto showLights = [this](std::vector<Light>& lights, sf::Vector2f pos, float spacingBetweenLights) {
				for (uint32_t index = 0; index < lights.size(); ++index)
				{
					lights[index].setPosition(pos.x, pos.y + index * lights[index].getRadius() * spacingBetweenLights);
					m_window->draw(lights[index]);
				}
			};
			showLights(lightsOn, { 50,200 }, spacingBetweenLights);
			showLights(lightsOff, { 700,200 }, spacingBetweenLights);
			if (on && off)
				return;


			m_window->draw(chooseOn);
			m_window->draw(chooseOff);
			m_window->display();
		}
	}

	void Player::CreateTable(Board& board)
	{
		sf::Sprite background;
		background.setTexture(m_background);
		background.setScale(m_window->getSize().x / (float)m_background.getSize().x, m_window->getSize().y / (float)m_background.getSize().y);

		Button done(1000, 350, 150, 150, sf::Color::Transparent, "Done");
		done.SetCallback([this]() {});

		sf::Text clickLights(" Click on the lights you want to change", m_font, 40);
		clickLights.setFillColor(sf::Color::White);
		clickLights.setPosition({ 150, 100 });

		sf::Text notSolvable("This board is not solvable!", m_font, 50);
		notSolvable.setFillColor(sf::Color::Red);
		notSolvable.setPosition({ 300,700 });

		bool check = true;

		while (m_window->isOpen())
		{
			sf::Event event;

			sf::Vector2f mousePosView = m_window.get()->mapPixelToCoords(sf::Mouse::getPosition(*m_window));
			while (m_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					m_window->close();

				done.Update(event);

				if (event.type == sf::Event::MouseButtonPressed)
					for (int line = 0; line < board.GetLights().size(); ++line)
						for (int column = 0; column < board.GetLights()[line].size(); ++column)
							if (board.GetElementAt({ line, column }).getGlobalBounds().contains(mousePosView))
								board.UpdateLight({ line, column });

				if (done.IsPressed())
				{
					if (GameUtilities<Board, Light>::SolvableBoard(board))
						return;
					check = false;
					done.ResetPressed();
				}
			}
			m_window->clear();
			m_window->draw(background);
			if (check == false)
				m_window->draw(notSolvable);
			m_window->draw(board);
			done.Draw(m_window);
			m_window->draw(clickLights);
			m_window->display();
		}
	}

	bool Player::ChooseFastMode()
	{
		bool option;
		Menu mainmenu;

		Button opt1("Yes", [&option]() { option = true; });
		mainmenu.AddOption(opt1);

		Button opt2("No", [&option]() {option = false; });
		mainmenu.AddOption(opt2);
		mainmenu.AddTitle("Do you want to play in fast mode?");
		mainmenu.SetLayout(MenuLayout::VerticleCentered);
		mainmenu.SetExitCallback([this]() {m_window->close(); });
		mainmenu.Display(m_window);
		return option;

	}

	bool Player::ChooseHowToPlay()
	{

		bool option;

		Menu menu;
		menu.AddTitle("Choose how to play:");
		menu.SetLayout(MenuLayout::VerticleCentered);

		Button createBoard("Create your own board", [&option]() { option = true; });
		menu.AddOption(createBoard);

		Button predefinedBoard("Play on an existing board", [&option]() { option = false; });
		menu.AddOption(predefinedBoard);

		menu.SetExitCallback([this]() {m_window->close(); });

		menu.Display(m_window);
		return option;
	}

	void Player::ReadPlayerName()
	{
		sf::Sprite background;
		background.setTexture(m_background);
		background.setScale(m_window->getSize().x / (float)m_background.getSize().x, m_window->getSize().y / (float)m_background.getSize().y);

		TextBox textBox(40, sf::Color::White, { 460, 270 }, 540, 100);
		textBox.SetLimit(true, 20);

		sf::Text enterNameText("Enter your name here:", m_font, 50);
		enterNameText.setFillColor(sf::Color::White);
		enterNameText.setPosition({ 350, 170 });

		sf::Text helloMessage;
		helloMessage.setFillColor(sf::Color::White);
		helloMessage.setPosition({ 540, 400 });
		helloMessage.setCharacterSize(50);
		helloMessage.setFont(m_font);

		Button continueButton(700, 600, 100, 100, sf::Color::Transparent, "Continue");
		continueButton.SetTextSize(55);
		continueButton.SetCallback([this]() {});

		while (m_window->isOpen())
		{
			sf::Event event;

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) && textBox.GetPressed())
			{
				textBox.SetPressed(false);
				helloMessage.setString("Hello, " + textBox.GetText() + "!");
			}

			while (m_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					m_window->close();

				continueButton.Update(event);

				if (event.type == sf::Event::TextEntered)
					textBox.WriteOnBox(event);

				if (textBox.IsPressed(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)))
					textBox.SetPressed(true);

				if (continueButton.IsPressed())
					return;

			}
			this->m_name.setString(textBox.GetText());

			m_window->clear();

			m_window->draw(background);
			m_window->draw(enterNameText);
			m_window->draw(helloMessage);
			continueButton.Draw(m_window);
			textBox.Draw(m_window);

			m_window->display();

		}
	}

	Board::Type Player::ChooseBoardType()
	{
		sf::Sprite background;
		background.setTexture(m_background);
		background.setScale(m_window->getSize().x / (float)m_background.getSize().x, m_window->getSize().y / (float)m_background.getSize().y);

		Button button3x3(230, 130, 310, 70, sf::Color::Transparent, "3x3");
		Button button5x5(1000, 130, 310, 80, sf::Color::Transparent, "5x5");
		Button button7x7(230, 400, 310, 80, sf::Color::Transparent, "7x7");
		Button button9x9(1000, 400, 310, 80, sf::Color::Transparent, "9x9");
		Button buttonNext(730, 430, 100, 50, sf::Color::Transparent, "Next");

		sf::Texture texture3x3, texture5x5, texture7x7, texture9x9;
		auto getSprite = [](sf::Texture& texture, const sf::Vector2f& pos, const std::string& path) {
			if (!texture.loadFromFile(path))
				throw std::runtime_error("Failed to load path");
			texture.setSmooth(true);
			sf::Sprite sprite;
			sprite.setPosition(pos);
			sprite.setTexture(texture);
			return sprite;
		};

		auto isSpritePressed = [](sf::Sprite& sprite, const sf::Event& event, sf::Vector2f mousePosView) {

			if ((sprite.getGlobalBounds().contains(mousePosView) && event.type == sf::Event::MouseButtonReleased && event.mouseButton.button == sf::Mouse::Left))
				return true;
			return false;
		};

		sf::Sprite board3x3 = std::move(getSprite(texture3x3, { 230,220 }, "../Resources/3.png")),
			board5x5 = std::move(getSprite(texture5x5, { 980,220 }, "../Resources/5.png")),
			board7x7 = std::move(getSprite(texture7x7, { 230,500 }, "../Resources/7.png")),
			board9x9 = std::move(getSprite(texture9x9, { 980,500 }, "../Resources/9.png"));

		TextBox textBox1(20, sf::Color::White, { 600, 400 }, 50, 50);
		textBox1.SetLimit(true, 1);

		TextBox textBox2(20, sf::Color::White, { 750, 400 }, 50, 50);
		textBox2.SetLimit(true, 1);

		sf::Text X("X", m_font, 50);
		X.setFillColor(sf::Color::White);
		X.setPosition(670, 380);

		sf::Text enterDimension("Enter dimenions here:", m_font, 30);
		enterDimension.setFillColor(sf::Color::White);
		enterDimension.setPosition({ 480, 250 });

		sf::Text text("between 3 and 9", m_font, 20);
		text.setFillColor(sf::Color::White);
		text.setPosition({ 580, 300 });

		sf::Text choose("Choose the board size you want to play:", m_font, 30);
		choose.setFillColor(sf::Color::Magenta);
		choose.setStyle(sf::Text::Bold | sf::Text::Italic);
		choose.setPosition({ 250, 70 });

		sf::Text length, width;
		std::string Lenght, Width;
		bool next = false, error = false;
		while (m_window->isOpen())
		{
			sf::Event event;
			sf::Vector2f mousePosView = m_window.get()->mapPixelToCoords(sf::Mouse::getPosition(*m_window));

			while (m_window->pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					m_window->close();

				button3x3.Update(event);
				button5x5.Update(event);
				button7x7.Update(event);
				button9x9.Update(event);
				buttonNext.Update(event);

				if (isSpritePressed(board3x3, event, mousePosView))
					return GameUtilities<Board, Light>::GetPredefinedType(0);
				if (isSpritePressed(board5x5, event, mousePosView))
					return GameUtilities<Board, Light>::GetPredefinedType(1);
				if (isSpritePressed(board7x7, event, mousePosView))
					return GameUtilities<Board, Light>::GetPredefinedType(2);
				if (isSpritePressed(board9x9, event, mousePosView))
					return GameUtilities<Board, Light>::GetPredefinedType(3);

				if (buttonNext.IsPressed() == true && next == true)
				{
					uint32_t lenghtBoard, widthBoard;
					std::stringstream sLenght(Lenght), sWidth(Width);
					sLenght >> lenghtBoard;
					sWidth >> widthBoard;
					return{ widthBoard,  lenghtBoard };
				}

				if (event.type == sf::Event::TextEntered)
				{
					textBox1.WriteOnBox(event);
					textBox2.WriteOnBox(event);
				}

				if (event.type == sf::Event::MouseButtonPressed)
				{
					textBox1.SetPressed(false);
					textBox2.SetPressed(false);
				}

				if (textBox1.IsPressed(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)))
				{
					textBox1.SetPressed(true);
					textBox2.SetPressed(false);
				}


				if (textBox2.IsPressed(sf::Vector2f(event.mouseButton.x, event.mouseButton.y)))
				{
					textBox2.SetPressed(true);
					textBox1.SetPressed(false);
				}

				length.setString(textBox2.GetText());
				width.setString(textBox1.GetText());
				if (length.getString().getSize() != 0 && width.getString().getSize() != 0)
				{
					Lenght = length.getString();
					Width = width.getString();
					error = true;
					next = false;
					if (std::regex_match(Lenght, std::regex("[3-9]")) && std::regex_match(Width, std::regex("[3-9]")))
					{
						next = true;
						error = false;
					}
				}
			}
			m_window->clear();
			m_window->draw(background);
			button3x3.Draw(m_window);
			button5x5.Draw(m_window);
			button7x7.Draw(m_window);
			button9x9.Draw(m_window);

			m_window->draw(board3x3);
			m_window->draw(board5x5);
			m_window->draw(board7x7);
			m_window->draw(board9x9);

			textBox1.Draw(m_window);
			textBox2.Draw(m_window);
			m_window->draw(enterDimension);
			m_window->draw(text);

			if (next == true)
				buttonNext.Draw(m_window);
			if (error)
			{
				sf::Text errorText("Invalid size", m_font, 40);
				errorText.setFillColor(sf::Color::Red);
				errorText.setStyle(sf::Text::Bold | sf::Text::Italic);
				errorText.setPosition({ 500, 450 });
				m_window->draw(errorText);
			}

			m_window->draw(X);

			m_window->draw(choose);

			m_window->display();
		}
		return Board::kErrorType;
	}

	void Player::Init(std::shared_ptr<sf::RenderWindow> window)
	{
		m_window = window;
		if (!m_background.loadFromFile("../Resources/bg2.jpg"))
			throw std::runtime_error("Failed to load bg2.jpg");
		if (!m_font.loadFromFile("../Resources/Virgo.ttf"))
			throw std::runtime_error("Failed to load Virgo.ttf");
	}

	sf::Text Player::GetName()
	{
		return m_name;
	}
}