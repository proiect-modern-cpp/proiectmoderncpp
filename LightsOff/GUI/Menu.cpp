#include "Menu.h"


const MenuLayout MenuLayout::VerticleCentered = MenuLayout(100, sf::Vector2f(0.50, 0.5));
const MenuLayout MenuLayout::VerticleLeft = MenuLayout(50, sf::Vector2f(0.50, 0.5));
const MenuLayout MenuLayout::VerticleRight = MenuLayout(50, sf::Vector2f(0.75, 0.5));

MenuLayout::MenuLayout(unsigned int spacing, const sf::Vector2f& origin) {
	SetSpacing(spacing);
	SetOrigin(origin);
}


MenuLayout::MenuLayout(const MenuLayout& other) {
	this->m_spacing = other.m_spacing;
	this->m_origin = other.m_origin;
}


void MenuLayout::SetSpacing(unsigned int spacing) {
	this->m_spacing = spacing;
}


void MenuLayout::SetOrigin(const sf::Vector2f& origin) {
	this->m_origin = origin;
	if (this->m_origin.x > 1.0) {
		this->m_origin.x = 1.0;
	}
	if (this->m_origin.y > 1.0) {
		this->m_origin.y = 1.0;
	}
}


void MenuLayout::Apply(std::vector<sf::Text>& options, const sf::Vector2u& windowSize)
{
	if (options.size() == 1) { return; } 

	sf::Vector2f pos; 
	pos.x = windowSize.x * this->m_origin.x;
	pos.y = (windowSize.y + 200) * this->m_origin.y;
	float yCoordTitle = (windowSize.y - 200) * this->m_origin.y;
	
	int noOfOptions = options.size();
	options[0].setCharacterSize(50);
	auto optionSize = options[1].getGlobalBounds();
	if (noOfOptions % 2) { 
		pos.y = pos.y - (int)(noOfOptions / 2) * (optionSize.height + m_spacing);
		yCoordTitle = yCoordTitle - (int)(noOfOptions / 2) * (options[0].getGlobalBounds().height);
	}
	else { 
		pos.y = pos.y - (m_spacing / 2.0f) - (optionSize.height / 2.0f) - (noOfOptions / 2 - 1) * (m_spacing + optionSize.height);
		yCoordTitle = yCoordTitle - (m_spacing / 2.0f) - (optionSize.height / 2.0f) - (noOfOptions / 2 - 1) * (options[0].getGlobalBounds().height);

	}
	options[0].setOrigin(options[0].getLocalBounds().left + options[0].getLocalBounds().width / 2.0f,
		options[0].getLocalBounds().top + options[0].getLocalBounds().height / 2.0f);
	options[0].setPosition(pos.x, yCoordTitle);

	for (int index = 1; index < options.size(); index++) 
	{
		options[index].setCharacterSize(40);
		sf::FloatRect rect = options[index].getLocalBounds();
		
		options[index].setOrigin(rect.left + rect.width / 2.0f,
			rect.top + rect.height / 2.0f);
		options[index].setPosition(pos); 
		pos.y += optionSize.height + m_spacing; 
	}
}

Menu::Menu()
{
	sf::Text text;
	if(!m_font.loadFromFile("../Resources/Virgo.ttf"))
		throw std::runtime_error("Failed to load Virgo.ttf");
	if(!m_background.loadFromFile("../Resources/bg2.jpg"))
		throw std::runtime_error("Failed to load bg2.jpg");
	text.setFont(m_font);
	this->SetTemplateText(text);
}

void Menu::Display(std::shared_ptr<sf::RenderWindow> window) {
	sf::Sprite background;
	background.setTexture(m_background);
	background.setScale(window->getSize().x / (float)m_background.getSize().x, window->getSize().y / (float)m_background.getSize().y);

	std::vector<sf::Text> texts;
	sf::Text text = this->m_templateText;

	for (const auto& option : m_options) {
		text.setString(option.GetText());
		texts.push_back(text);
	}
	
	window->clear();
	window->draw(background);
	m_layout.Apply(texts, window->getSize()); 

	for (auto& option : texts)
		window->draw(option);
	window->display();

	bool finished = false;
	uint32_t index = 0;
	sf::Vector2i mousePos;

	while (!finished) {
		sf::Event event;

		while (window->pollEvent(event)) {

			switch (event.type) {

			case sf::Event::Closed:
				finished = true;
				if (m_exitCallback)
					m_exitCallback();
				window->close();
				break;

			case sf::Event::GainedFocus:
				window->clear();
				window->draw(background); 
				for (auto& option : texts)
					window->draw(option);
				window->display();
				break;

			case sf::Event::MouseButtonReleased:
				for (index = 0; index < texts.size(); index++) {
					if (texts[index].getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y)) {
						m_options[index].Select();
						finished = true;
						break;
					}
				}
				break;

			default:
				break;
			}
		}

	}
}


void Menu::AddOption(const Button& option) {
	m_options.push_back(option);
}

void Menu::AddTitle(const std::string& title)
{
	Button btitle;
	btitle.SetText(title);
	m_options.emplace(m_options.begin(), title);
}

void Menu::ClearOptions() {
	m_options.clear();
}

void Menu::SetBackground(const std::string& path) {
	m_background.loadFromFile(path);
}

void Menu::SetExitCallback(std::function<void(void)> callback) {
	m_exitCallback = callback;
}

void Menu::SetLayout(const MenuLayout& layout) {
	this->m_layout = layout;
}

void Menu::SetTemplateText(const sf::Text& text) {
	m_templateText = text;
}