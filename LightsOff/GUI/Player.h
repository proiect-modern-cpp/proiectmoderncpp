#pragma once
#include"Board.h"
#include <iostream>
#include <string>
#include<fstream>
#include <SFML/Graphics.hpp>
#include "Button.h"
#include"../LightsOff/GameUtilities.h"
#include "Menu.h"
#include"TextBox.h"

namespace gui {
	class Player
	{
	public:
		Player() = default;
		Player(sf::Text);
		~Player() = default;
		uint32_t ChooseGameMode();
		void ChooseColors(Board&);

		void CreateTable(Board&);
		bool ChooseFastMode();

		bool ChooseHowToPlay();
		void ReadPlayerName();
		Board::Type ChooseBoardType();

		void Init(std::shared_ptr < sf::RenderWindow>);

		sf::Text GetName();
	private:
		sf::Text m_name;
		sf::Font m_font;
		sf::Texture m_background;
		std::shared_ptr<sf::RenderWindow> m_window;
	};
}