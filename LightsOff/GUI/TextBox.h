#pragma once
#include <SFML/Graphics.hpp>
#include <sstream>

class TextBox
{
public:
	TextBox() = default;
	TextBox(int, const sf::Color&, const sf::Vector2f&, int, int);

	void SetPressed(bool);

	void SetLimit(bool, int);

	std::string GetText() const;
	bool GetPressed() const;

	void Draw(std::shared_ptr<sf::RenderWindow>) const;
	void WriteOnBox(sf::Event);
	bool IsPressed(const sf::Vector2f&) const;

private:
	sf::Text m_textBox;
	sf::Font m_font;
	sf::RectangleShape m_box;
	std::ostringstream m_stream;
	bool m_isPressed = false;
	bool m_hasLimit = false;
	int m_limit = 0;

	void VerifyInput(int);
	void DeleteLastChar();
};