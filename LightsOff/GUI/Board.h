#pragma once
#include <map>
#include <vector>
#include <functional>
#include "Light.h"

namespace gui {

	class Board : public sf::Drawable
	{
	public:
		Board();
		~Board() = default;
		using Position = std::pair<uint8_t, uint8_t>;
		using Type = std::pair<uint8_t, uint8_t>;

		void SetColorForLightOn(const sf::Color&);
		void SetColorForLightOff(const sf::Color&);

		Light& GetElementAt(std::pair<uint32_t, uint32_t>);

		std::vector<std::vector<Light>> GetLights();

		friend std::istream& operator >> (std::istream&, Board&);

		void SetTimeToResolveBoard(const double& time = 90.00);

		Board& UpdateLight(const Position&);
		Board& UpdateUpLight(const Position&);
		Board& UpdateDownLight(const Position&);
		Board& UpdateLeftLight(const Position&);
		Board& UpdateRightLight(const Position&);
		Board& UpdateDiagonalUpRightLight(const Position&);
		Board& UpdateDiagonalUpLeftLight(const Position&);
		Board& UpdateDiagonalDownRightLight(const Position&);
		Board& UpdateDiagonalDownLeftLight(const Position&);

		void EmptyBoard(const Type&);
		bool Finish();
		void DeleteBoard();
		double& GetTimeToResolveBoard();

		void PositionOnWindow(const sf::Vector2u&);

		static constexpr Type kErrorType = { 0,0 };

	private:

		std::vector<std::vector<Light>> m_lights;
		uint32_t m_CountLightsOn;
		double m_timeToResolveBoard;
		sf::Color m_colorLightOn, m_colorLightOff;

		void CheckLightsOn(const std::pair<uint32_t, uint32_t>&);
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	};

}