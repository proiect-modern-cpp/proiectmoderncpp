#include "Game.h"
#include <SFML/Audio.hpp>
#include <sstream>
#include <fstream>

namespace gui {
	Logger logger(std::cout);

	Game::Game(std::shared_ptr<sf::RenderWindow> window)
	{
		m_window = window;
		if (!m_backgroundTexture.loadFromFile("../Resources/bg2.jpg"))
			throw std::runtime_error("Failed to load bg2.jpg");
		if (!m_font.loadFromFile("../Resources/Virgo.ttf"))
			throw std::runtime_error("Failed to load Virgo.ttf");

		m_background.setTexture(m_backgroundTexture);
		m_background.setScale(m_window->getSize().x / (float)m_backgroundTexture.getSize().x, m_window->getSize().y / (float)m_backgroundTexture.getSize().y);

		m_changeBoard.ConfigureButton({ 1050, 700 }, { 260, 65, }, "Change board", sf::Color::Transparent);
		m_changePlayer.ConfigureButton({ 540, 700 }, { 260, 65, }, "Change Player", sf::Color::Transparent);
		m_resetBoard.ConfigureButton({ 1050, 350 }, { 260,65 }, "Reset board", sf::Color::Transparent);
		m_reconfigure.ConfigureButton({ 50, 700 }, { 260, 65, }, "Reconfigure", sf::Color::Transparent);

		m_reconfigure.SetCallback([this]() {
			std::stringstream logStream;
			logStream << "The user har chosen to reconfigure the game";
			logger.log(logStream.str(), Logger::Level::Info);
			m_board.DeleteBoard();
			Run(false); });
		m_changePlayer.SetCallback([this]() {
			std::stringstream logStream;
			logStream << "The user has chosen to change the player";
			logger.log(logStream.str(), Logger::Level::Info);
			m_board.DeleteBoard();
			Run(true); });
	}

	void Game::GameLoop(uint32_t gameMode, const Board::Type& type, bool fastMode)
	{
		auto getTime = [](const unsigned int seconds) {
			std::string min, sec;
			min = std::to_string(seconds / 60);
			if (min.size() == 1)
			{
				min.push_back(min[0]);
				min[0] = '0';
			}
			sec = std::to_string(seconds % 60);
			if (sec.size() == 1)
			{
				sec.push_back(sec[0]);
				sec[0] = '0';
			}
			return min + ":" + sec;
		};

		sf::Clock Clock;
		sf::Time time;
		sf::Text timer("00:00", m_font, 50);
		timer.setFillColor(sf::Color::White);
		timer.setStyle(sf::Text::Bold);
		timer.setPosition(sf::Vector2f(m_window->getSize().x * 0.5 - timer.getCharacterSize() * (timer.getString().getSize() - 1) / 2, 28));

		sf::RectangleShape outline;
		outline.setSize(sf::Vector2f(timer.getGlobalBounds().width + 40, timer.getGlobalBounds().height + 30));
		outline.setFillColor(sf::Color(0, 0, 0, 130));
		outline.setPosition(timer.getPosition().x - 20, timer.getPosition().y);

		Board aux = m_board;

		m_changeBoard.SetCallback([&gameMode, &type, &fastMode, this]() 
		{
			std::stringstream logStream;
			logStream << "The user har chosen to change the board";
			logger.log(logStream.str(), Logger::Level::Info); 
			ChangeBoard(gameMode, type, fastMode); 
		});
		m_resetBoard.SetCallback([this, &aux, &time, &timer]() {
			std::stringstream logStream;
			logStream << "The user has chosen to reset the board";
			logger.log(logStream.str(), Logger::Level::Info);
			m_board = aux;
			time = sf::Time::Zero;
			timer.setFillColor(sf::Color::White);
			});

		while (m_window->isOpen())
		{
			sf::Vector2f mousePosView = m_window.get()->mapPixelToCoords(sf::Mouse::getPosition(*m_window));
			while (m_window->pollEvent(this->m_event))
			{
				m_resetBoard.Update(this->m_event);
				m_changeBoard.Update(this->m_event);
				m_reconfigure.Update(this->m_event);
				m_changePlayer.Update(this->m_event);

				if (!m_window->isOpen())
					return;

				if (m_event.type == sf::Event::Closed)
					m_window->close();
				else
				{
					if (m_event.type == sf::Event::MouseButtonPressed)
					{
						for (int line = 0; line < m_board.GetLights().size(); ++line)
						{
							for (int column = 0; column < m_board.GetLights()[line].size(); ++column)
							{
								if (m_board.GetElementAt({ line, column }).getGlobalBounds().contains(mousePosView))
								{
									GameUtilities<Board, Light>::ApplyRule(m_board, gameMode, { line,column });
								}
							}
						}
					}
				}
			}

			time += Clock.restart();
			const unsigned int seconds = static_cast<unsigned int>(time.asSeconds());
			timer.setString(getTime(seconds));
			if (seconds >= m_board.GetTimeToResolveBoard() && fastMode == true)
				timer.setFillColor(sf::Color::Red);

			m_window->clear();
			m_window->draw(m_background);
			m_window->draw(outline);
			m_window->draw(timer);
			m_window->draw(m_board);
			m_resetBoard.Draw(m_window);
			m_changeBoard.Draw(m_window);
			m_reconfigure.Draw(m_window);
			m_changePlayer.Draw(m_window);

			std::stringstream logStream;

			if (m_board.Finish())
			{
				logStream << "The game lasted " << seconds << " seconds! ";
				if (seconds < m_board.GetTimeToResolveBoard() || fastMode == false)
				{
					logStream << "Game won!";
					logger.log(logStream.str(), Logger::Level::Info);
					GameWon(gameMode, type, fastMode, aux);
					return;
				}
				else
				{
					logStream << "Game lost!";
					logger.log(logStream.str(), Logger::Level::Info);
					GameLost(gameMode, type, fastMode, aux);
					return;
				}
			}
			m_window->display();
		}
	}

	void Game::Run(bool option)
	{
		m_player.Init(m_window);

		std::stringstream logStream;

		if (option)
			m_player.ReadPlayerName();

		if (!m_window->isOpen())
			return;

		std::string playerName;
		if (m_player.GetName().getString().getSize() != 0)
		{
			playerName.append("the username ");
			playerName.append(m_player.GetName().getString().toAnsiString());
		}
		else
			playerName.append("no username");

		m_player.ChooseColors(m_board);
		if (!m_window->isOpen())
			return;

		logStream << "The user with " << playerName << " has chosen the colors for the lights.";
		logger.log(logStream.str(), Logger::Level::Info);
		logStream.str("");

		Board::Type type;
		uint32_t gameMode;

		if (m_player.ChooseHowToPlay())
		{
			logStream << "The user with " << playerName << " has chosen to create his own board.";
			logger.log(logStream.str(), Logger::Level::Info);
			logStream.str("");

			if (!m_window->isOpen())
				return;

			m_board.DeleteBoard();
			m_board.EmptyBoard(GameUtilities<Board, Light>::GetPredefinedType(1));
			m_board.PositionOnWindow(m_window.get()->getSize());
			m_player.CreateTable(m_board);

			if (!m_window->isOpen())
				return;

			type = GameUtilities<Board, Light>::GetPredefinedType(1);
			gameMode = kStandardGameMode;
		}
		else
		{
			logStream << "The user with " << playerName << " has chosen to play on a predefined board.";
			logger.log(logStream.str(), Logger::Level::Info);
			logStream.str("");

			type = std::move(m_player.ChooseBoardType());
			if (type == Board::kErrorType)
				return;
			if (!m_window->isOpen())
				return;
			gameMode = std::move(m_player.ChooseGameMode());
			if (!m_window->isOpen())
				return;

			std::string gameOption = std::move(GameUtilities<Board, Light>::GetGameMode(gameMode));
			logStream << "The user with " << playerName << " has chosen to play a " << static_cast<int>(type.first) << "x" << static_cast<int>(type.second) << " game with" << gameOption;
			logger.log(logStream.str(), Logger::Level::Info);
			logStream.str("");

			if (!m_window->isOpen())
				return;
			if (GameUtilities<Board, Light>::IsPredefinedType(type) && gameMode == kStandardGameMode)
				GameUtilities<Board, Light>::GetBoard(m_board, type);
			else
				GameUtilities<Board, Light>::CreateUndefinedBoard(m_board, type, gameMode);
			m_board.PositionOnWindow(m_window.get()->getSize());
		}

		if (!m_window->isOpen())
			return;

		bool fastModeOption = std::move(m_player.ChooseFastMode());
		if (!m_window->isOpen())
			return;
		if (fastModeOption)
		{
			logStream << "The user with " << playerName << " has chosen to play in fast mode.";
			logger.log(logStream.str(), Logger::Level::Info);
		}

		GameLoop(gameMode, type, fastModeOption);

	}

	void Game::ChangeBoard(uint32_t gameMode, const Board::Type& type, bool fastMode)
	{
		m_board.DeleteBoard();
		if (GameUtilities<Board, Light>::IsPredefinedType(type) && gameMode == kStandardGameMode)
			GameUtilities<Board, Light>::GetBoard(m_board, type);
		else
			GameUtilities<Board, Light>::CreateUndefinedBoard(m_board, type, gameMode);
		m_board.PositionOnWindow(m_window.get()->getSize());
		GameLoop(gameMode, type, fastMode);
	}

	void Game::GameLost(uint32_t gameMode, const Board::Type& type, bool fastMode, const Board& aux)
	{
		sf::SoundBuffer buffer;
		if (!buffer.loadFromFile("../Resources/lost.wav"))
			throw std::runtime_error("Failed to load lost.wav");
		sf::Sound sound;
		sound.setBuffer(buffer);
		sound.setVolume(50);
		sound.play();

		Board board;
		board.SetColorForLightOff(sf::Color::Black);
		board.SetColorForLightOn(sf::Color::Magenta);
		board.EmptyBoard(GameUtilities<Board, Light>::GetPredefinedType(2));
		for (auto pos : kSadFace)
		{
			board.UpdateLight(pos);
		}

		m_changeBoard.SetCallback([&gameMode, &type, &fastMode, this]() {
			std::stringstream logStream;
			logStream << "The user har chosen to change the board";
			logger.log(logStream.str(), Logger::Level::Info);
			ChangeBoard(gameMode, type, fastMode); });
		m_resetBoard.SetCallback([this, &aux, gameMode, type, fastMode]() {
			std::stringstream logStream;
			logStream << "The user has chosen to reset the board";
			logger.log(logStream.str(), Logger::Level::Info);
			m_board = aux;
			GameLoop(gameMode, type, fastMode); });


		sf::Text youLostText;
		youLostText.setFillColor(sf::Color::White);
		youLostText.setString("You lost...");
		youLostText.setPosition(sf::Vector2f(m_window->getSize().x * 0.5 - youLostText.getCharacterSize() * youLostText.getString().getSize() / 2, 30));
		youLostText.setFont(m_font);
		youLostText.setCharacterSize(50);

		board.PositionOnWindow(m_window.get()->getSize());

		while (m_window->isOpen())
		{
			while (m_window->pollEvent(this->m_event))
			{
				if (m_event.type == sf::Event::Closed)
					m_window->close();
				m_changeBoard.Update(this->m_event);
				m_reconfigure.Update(this->m_event);
				m_changePlayer.Update(this->m_event);
				m_resetBoard.Update(this->m_event);
			}

			m_window->clear();
			m_window->draw(m_background);
			m_changeBoard.Draw(m_window);
			m_reconfigure.Draw(m_window);
			m_changePlayer.Draw(m_window);
			m_resetBoard.Draw(m_window);
			m_window->draw(board);
			m_window->draw(youLostText);
			m_window->display();
		}
	}

	void Game::GameWon(uint32_t gameMode, const Board::Type& type, bool fastMode, const Board& aux)
	{
		sf::SoundBuffer buffer;
		if (!buffer.loadFromFile("../Resources/applause.wav"))
			throw std::runtime_error("Failed to load applause.wav");
		sf::Sound sound;
		sound.setBuffer(buffer);
		sound.setVolume(50);
		sound.play();

		Board board = aux;
		board.PositionOnWindow(m_window.get()->getSize());
		std::vector<sf::Color> colors;
		colors.push_back(sf::Color::Red);
		colors.push_back(sf::Color::Yellow);
		colors.push_back(sf::Color::Cyan);
		colors.push_back(sf::Color::Blue);
		colors.push_back(sf::Color::Green);
		colors.push_back(sf::Color::Magenta);

		sf::Text congratsText;
		congratsText.setFillColor(sf::Color::White);
		congratsText.setString("Congratulations!!!");
		congratsText.setPosition(sf::Vector2f(m_window->getSize().x * 0.5 - congratsText.getCharacterSize() * congratsText.getString().getSize() / 2, 30));
		congratsText.setFont(m_font);
		congratsText.setCharacterSize(50);

		m_changeBoard.SetCallback([&gameMode, &type, &fastMode, this]() {
			std::stringstream logStream;
			logStream << "The user har chosen to change the board";
			logger.log(logStream.str(), Logger::Level::Info);
			ChangeBoard(gameMode, type, fastMode); });
		m_resetBoard.SetCallback([this, &aux, gameMode, type, fastMode]() {
			std::stringstream logStream;
			logStream << "The user has chosen to reset the board";
			logger.log(logStream.str(), Logger::Level::Info);
			m_board = aux;
			GameLoop(gameMode, type, fastMode); });


		while (m_window->isOpen())
		{
			sf::Vector2f mousePosView = m_window.get()->mapPixelToCoords(sf::Mouse::getPosition(*m_window));
			while (m_window->pollEvent(this->m_event))
			{
				if (m_event.type == sf::Event::Closed)
					m_window->close();
				m_changeBoard.Update(this->m_event);
				m_reconfigure.Update(this->m_event);
				m_changePlayer.Update(this->m_event);
				m_resetBoard.Update(this->m_event);

			}
			for (uint32_t line = 0; line < board.GetLights().size(); ++line)
			{
				for (uint32_t column = 0; column < board.GetLights()[line].size(); ++column)
				{
					uint32_t indexOn, indexOff;
					do
					{
						indexOn = rand() % colors.size();
						indexOff = rand() % colors.size();
					} while (indexOn == indexOff);
					board.GetElementAt({ line, column }).UpdateLight(colors[indexOn], colors[indexOff]);
				}
			}

			m_window->clear();
			m_window->draw(m_background);
			m_changeBoard.Draw(m_window);
			m_reconfigure.Draw(m_window);
			m_changePlayer.Draw(m_window);
			m_resetBoard.Draw(m_window);
			m_window->draw(board);
			m_window->draw(congratsText);
			m_window->display();
		}
	}
}