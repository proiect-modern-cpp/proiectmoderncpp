# Lights Off
An extended version of the original game Lights Off / Lights Out.

# How to play

The objective is to turn all the lights off by pushing the fewest number of buttons.

## This version has more game board configurations:

1.  the game board size can be anywhere between 3 and 9
2.  the game rule can be one of the following:

*  classic (up, down, left, right)
*  up and down
*  left and right
*  diagonal up right and diagonal down left
*  diagonal up left and diagonal down right
*  up-down-left-right and diagonals

# Other features:

* CLI and GUI versions
* Logger
* Timer